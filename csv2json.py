import os
import sys
import csv
import codecs
import argparse
import simplejson as json


def convert(filename, delimiter=',', encoding='utf-8', indent=None):
    with codecs.open(filename, 'r', encoding=encoding) as infile:
        
        reader = csv.DictReader(infile, delimiter=delimiter)
        name, extension = os.path.splitext(filename)
        outname = "{}.{}".format(name, 'json')

        with codecs.open(outname, 'w', encoding=encoding) as outfile:
            data = (line for line in reader)
            
            json.dump(
                data, outfile, 
                indent=indent,
                ensure_ascii=False, 
                iterable_as_array=True
            )


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Convert CSV to JSON")
    parser.add_argument('-d', '--delimiter', help="CSV delimiter")
    parser.add_argument('-e', '--encoding', help="CSV encoding")
    parser.add_argument('-i', '--indent', type=int, help="JSON indent")
    parser.add_argument('files', nargs='+')
    args = parser.parse_args()

    opts = {}
      
    if args.delimiter:
        opts.update({'delimiter': args.delimiter})
    if args.encoding:
        opts.update({'encoding': args.encoding})
    if args.indent:
        opts.update({'indent': args.indent})

    for filename in args.files:
        convert(filename, **opts)

